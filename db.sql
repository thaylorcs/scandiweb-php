DROP DATABASE IF EXISTS shop;
CREATE DATABASE IF NOT EXISTS shop;

USE shop;

CREATE TABLE IF NOT EXISTS products
(
    product_id         INTEGER AUTO_INCREMENT PRIMARY KEY,
    sku                VARCHAR(20) UNIQUE                    NOT NULL,
    price              DECIMAL(10, 2)                        NOT NULL,
    product_title      VARCHAR(100)                          NOT NULL,
    product_type       INTEGER                               NOT NULL,
    product_attributes VARCHAR(30) DEFAULT '1',
    created_at         TIMESTAMP   DEFAULT CURRENT_TIMESTAMP NOT NULL
);


INSERT INTO `products` (`product_id`, `sku`, `price`, `product_title`, `product_type`, `created_at`)
VALUES (1, 'abc-def-ghi', '10.20', 'Avengers Endgame', 1, '2022-01-03 18:01:54'),
       (2, 'abc-def-jkl', '10.30', 'The Hunger Games', 2, '2022-01-03 18:01:54'),
       (3, 'abc-def-mno', '10.50', 'Wooden Chair', 3, '2022-01-03 18:01:54'),
       (4, 'abc-def-pqr', '30.50', 'Spiderman: No way home', 1, '2022-01-03 18:01:55'),
       (5, 'abc-def-stu', '22.50', 'Spiderman: Homecoming', 1, '2022-01-03 18:01:55'),
       (6, 'abc-def-vwx', '22.50', 'Spiderman: Far from home', 1, '2022-01-03 18:01:55'),
       (7, 'abc-def-zab', '22.50', 'Avengers: Infinity War', 1, '2022-01-03 18:01:55'),
       (8, 'abc-def-zbc', '22.50', 'The Lord of The Rings: The Two Towers', 1, '2022-01-03 18:01:55'),
       (9, 'abc-def-zcd', '22.50', 'Star Wars Episode IV: A new Hope', 1, '2022-01-03 18:01:55'),
       (10, 'abc-def-zef', '22.50', 'Star Wars Episode V: The Empire Strikes Back', 1, '2022-01-03 18:01:55'),
       (12, 'abc-def-zfg', '23.50', 'Star Wars Episode VI: Return of the Jedi', 1, '2022-01-15 13:05:26'),
       (13, 'abc-deg-abc', '23.50', 'Wooden Sofa', 3, '2022-01-15 13:10:40'),
       (15, 'aaa-aaa-aaa', '300.00', 'Wooden Table', 3, '2022-01-15 14:19:01'),
       (16, '213435465', '34.00', 'Spider-Man Movie', 1, '2022-01-15 14:20:57'),
       (17, '456123789', '34.00', 'Spider-Man Movie', 1, '2022-01-15 14:25:04'),
       (18, 'aba-aaa-aaa', '20.00', 'Guardians of The Galaxy', 2, '2022-01-15 15:03:05'),
       (19, 'dac-dac-bac', '5.20', 'Guardians of The Galaxy 2', 1, '2022-01-15 15:09:16'),
       (20, 'aaa-bla-bla', '30.45', 'Guardians of The Galaxy 3', 1, '2022-01-15 15:18:34'),
       (21, 'aba-bab-dak', '30.41', 'Matrix Ressurections', 1, '2022-01-15 15:21:22'),
       (22, 'bac-bac-bac', '30.00', 'Matrix', 1, '2022-01-15 17:10:33'),
       (23, 'aba-cad-dac', '20.00', 'A quiet place', 1, '2022-01-15 23:14:11'),
       (26, '000-000-001', '20.00', 'Holy Bible', 2, '2022-01-16 14:52:10');


UPDATE products
SET product_attributes = '700MB'
where product_type = '1';
UPDATE products
SET product_attributes = '0.2Kg'
WHERE product_type = 2;
UPDATE products
SET product_attributes = '120x100x75'
WHERE product_type = 3;


CREATE TABLE IF NOT EXISTS product_types
(
    type_id   INTEGER AUTO_INCREMENT PRIMARY KEY,
    type_name VARCHAR(30)
);

INSERT INTO `product_types` (`type_id`, `type_name`)
VALUES (1, 'DVD'),
       (2, 'Book'),
       (3, 'Furniture');
