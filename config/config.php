<?php

require 'DotEnv.php';

(new DotEnv(__DIR__ . '/.env'))->load();

define("Scandiweb\Config\HOST", getenv('DATABASE_HOST'));
define("Scandiweb\Config\DB_USER", getenv("DATABASE_USER"));
define("Scandiweb\Config\PASSWORD", getenv("DATABASE_PASSWORD"));
define("Scandiweb\Config\DB_NAME", getenv("DATABASE_NAME"));