//Dynamic form
function changeProductInfo() {
    const productType = document.querySelector("#productType");
    let attributes = document.querySelector("#attributes");
    var selected = productType.value;

    switch (selected) {
        case "1":
            attributes.innerHTML = `
                <label for="size">Size</label>
                <input type="number" step="1" name="size" required class="form-control"  id="size" placeholder="Please inform the DVD Size (in MB)" >`
            break;
        case "2":
            attributes.innerHTML = `
                <label for="weight">Weight</label>
                <input type="number" step="0.01" name="weight" required class="form-control" id="weight" placeholder="Please inform the book  Weight (in Kg)">`
            break;
        case "3":
            attributes.innerHTML = ` 
                <label for="height">Height</label>
                <input type="number" step="0.01" name="height" required class="form-control" id="height" placeholder="Please inform the furniture Height (in CM)">
                <label for="width">Width</label>
                <input type="number" step="0.01" name="width" required class="form-control" id="width" placeholder="Please inform the furniture Width (in CM)">
                <label for="length">Length</label>
                <input type="number" step="0.01" name="length" required class="form-control" id="length" placeholder="Please inform the furniture Length (in CM)">`
            break;
    }
}

const form = document.querySelector(".product_form");

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const sku = e.target.querySelector("#sku").value;
    const name = e.target.querySelector("#name").value;
    const price = e.target.querySelector("#price").value;
    const type = e.target.querySelector("#productType").value;
    var attributes = "";
    switch (type) {
        case "1":
            const size = e.target.querySelector("#size").value;
            attributes = size.toString() + "MB";
            break;
        case "2":
            const weight = e.target.querySelector("#weight").value;
            attributes = weight.toString() + "Kg";
            break;
        case "3":
            const height = e.target.querySelector("#height").value;
            const width = e.target.querySelector("#width").value;
            const length = e.target.querySelector("#length").value;
            attributes = height.toString() + "x" + width.toString() + "x" + length.toString();
            break;
    }

    createProduct(sku, name, price, type, attributes)
        .then(() => {
            window.location.href = "../public/index.php"
        })
})


async function createProduct(sku, name, price, type, attributes) {
    return await fetch(`http://localhost:8080/products/create`, {
        method: 'POST',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            sku: sku,
            product_title: name,
            price: price,
            product_type: type,
            product_attributes: attributes
        })
    })
        .then(response => {
            return response.body
        })
}