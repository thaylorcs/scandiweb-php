document.getElementById("addButton").onclick = () => {
    location.href = "/public/add-product.php";
}

deleteBtn = document.querySelector("#delete-product-btn");
deleteBtn.onclick = () => {
    const checked = [...document.querySelectorAll('.delete-checkbox:checked')].map(e => e.value);
    checked.forEach(item => {
        deleteProduct(item);
        document.location.reload(true);
    })
}

const createProductDiv = (sku, title, price, id, type, attributes) => {
    const productDiv = document.createElement('div');
    productDiv.innerHTML = `
            <div class="product" >
                <input type="checkbox" name="delete-checkbox" class="delete-checkbox" value="${id}">
                <h2 class="product-id">
                    ${sku}
                </h2>
                <p class="product-title">${title}</p>
                <p class="product-price">$${price}</p>
                <p class="product-price">
                ${type == 1 ? 'Size: ' : type == 2 ? 'Weight: ' : 'Dimension: '}${attributes}</p>
            </div>`;
    return productDiv;
}

const productsList = document.querySelector(".products-list");

function getProducts() {
    fetch('http://localhost:8080/products/list', {
        method: 'GET', headers: {
            'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST,GET, OPTIONS, PUT',
        }
    })
        .then((response) => response.json())
        .then((data) => {
            data.forEach(product => {
                productsList.appendChild(createProductDiv(product.sku, product.product_title, product.price, product.product_id, product.product_type, product.product_attributes));
            })
        }).catch((error) => {
        console.log(error)
    })

}

getProducts();

function getChecked(deleteCheckbox) {
    var checkboxes = document.getElementsByName(deleteCheckbox);
    var checked = [];
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checked.push(checkboxes[i]);
        }
    }

    return checked.length > 0 ? checked : null;
}

const deleteProduct = (id) => {
    return fetch(`http://localhost:8080/products/delete/${id}`, {
        method: 'DELETE'
    })
}