<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/public/css/style.css">
    <link rel="shortcut icon" href="https://scandiweb.com/assets/images/apple-touch-icon.png">
    <title>Scandiweb Test Assignment</title>
</head>
<body>