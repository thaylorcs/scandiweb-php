<?php
require_once __DIR__ . '/header.php';
?>
<header class="top">
    <div class="logo">
        <p>Add Product </p>
    </div>
    <div class="top-buttons">
        <button type="submit" form="product_form" value="Save"
                class="btn btn-primary btn-submit save-btn">Save
        </button>
        <a href="index.php" class="btn btn-cancel">Cancel</a>
    </div>
</header>
<div class="product-info">
    <form action="add-product.php" id="product_form" class="product_form" method="POST">
        <label for="sku">SKU</label>
        <input type="text" id="sku" placeholder="Please inform the product SKU (should be unique)" name="sku"
               aria-label="product sku" required>
        <label for="name">Product Name</label>
        <input type="text" id="name" placeholder="Please inform the product Title" aria-label="product name"
               name="product-title" required>
        <label for="price">Price</label>
        <input type="number" id="price" placeholder="Please inform the product Price" aria-label="product price"
               name="price" min="0" step="0.01" required>
        <label for="productType">Select the product type</label>
        <select name="type" id="productType" onchange="changeProductInfo()" required>
            <option value="1" id="DVD">DVD</option>
            <option value="2" id="Book">Book</option>
            <option value="3" id="Furniture">Furniture</option>
        </select>
        <div class="form-group" id="attributes">
            <label for="size">Size</label>
            <input type="number" step="1" class="form-control" name="size" required
                   placeholder="Please inform the DVD Size (in MB)" id="size">
            <p class="error-message hidden">Please provide a Size for the DVD</p>
        </div>
    </form>
</div>

<?php require_once 'footer.php' ?>
<script src="js/add-product.js"></script>
</body>

</html>