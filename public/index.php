<?php
require_once __DIR__ . '/header.php';
?>

<header class="top">
    <div class="logo">
        <p>Product List</p>
    </div>
    <div class="top-buttons">
        <button id="addButton" class="btn">ADD</button>
        <a class="btn btn-delete" id="delete-product-btn">MASS DELETE</a>
    </div>
</header>
<main class="products-list" id="products-list">

</main>

<?php
require_once 'footer.php';
?>
<script defer src="/public/js/index.js"></script>
</body>

</html>
