<?php

use Scandiweb\Controllers\JsonController;
use Scandiweb\Controllers\RequestController;
use Scandiweb\Routes\Routes;

require 'autoload.php';
//require __DIR__ . "/vendor/autoload.php";

try {
    $requestController = new RequestController(Routes::getRoutes());
    $response = $requestController->handleRequest();
    $jsonController = new JsonController();
    $jsonController->arrayToJson($response);
} catch (Exception $exception) {
    return $exception->getMessage();
}
