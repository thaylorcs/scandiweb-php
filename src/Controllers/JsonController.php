<?php

namespace Scandiweb\Controllers;

class JsonController
{
    public function arrayToJson($response)
    {
        $data = [];

        if ((is_array($response) && count($response) > 0) || strlen($response) > 10) {
            $data = $response;
        }

        $this->returnJson($data);
    }

    private function returnJson($data)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS, DELETE');
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    public static function jsonBody()
    {
        $postJson = json_decode(file_get_contents('php://input'), true);

        if (is_array($postJson) && count($postJson) > 0) {
            return $postJson;
        }

        return "Empty Json";
    }
}