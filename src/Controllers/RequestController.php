<?php

namespace Scandiweb\Controllers;

use InvalidArgumentException;
use Scandiweb\Service\ProductService;

class RequestController
{
    private $request;
    private array $requestData = [];

    const REQUEST_TYPE = ['GET', 'POST', 'DELETE', 'OPTION'];
    const PRODUCTS = 'products';
    const REQ_RESOURCE = ['products'];
    const GET = 'GET';
    const DELETE = 'DELETE';

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function handleRequest()
    {
        $response = 'Route not allowed';
        if (in_array($this->request['method'], self::REQUEST_TYPE)) {
            $response = $this->sendRequest();
        }

        return $response;
    }

    public function sendRequest()
    {
        if ($this->request['method'] !== self::GET && $this->request['method'] !== self::DELETE) {
            $this->requestData = JsonController::jsonBody();
        }

        $method = $this->request['method'];
        return $this->$method();
    }

    private function get()
    {
        $response = "Invalid Route";
        if (in_array($this->request['route'], self::REQ_RESOURCE)) {
            switch ($this->request['route']) {
                case self::PRODUCTS:
                    $productService = new ProductService($this->request);
                    $response = $productService->validateGet();
                    json_encode($response);
                    break;
                default:
                    throw new InvalidArgumentException("Resource not found");
            }
        }
        return $response;
    }

    private function delete()
    {
        if (in_array($this->request['route'], self::REQ_RESOURCE)) {
            switch ($this->request['route']) {
                case self::PRODUCTS:
                    $productService = new ProductService($this->request);
                    $response = $productService->validateDelete();
                    json_encode($response);
                    break;
                default:
                    throw new InvalidArgumentException("Resource not found");
            }
        }
    }

    private function post()
    {
        if (in_array($this->request['route'], self::REQ_RESOURCE)) {
            switch ($this->request['route']) {
                case self::PRODUCTS:
                    $productService = new ProductService($this->request);
                    $productService->setRequestBody($this->requestData);
                    $response = $productService->validatePost();
                    break;
                default:
                    throw new InvalidArgumentException("Resource not found");
            }
            return $response;
        }
        throw new InvalidArgumentException("Invalid route");
    }
}