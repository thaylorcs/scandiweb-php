<?php

namespace Scandiweb\Entities;

class DVD extends Product
{
    private int $size;

    public function __construct(string $sku, string $name, float $price, $size)
    {
        parent::__construct($sku, $name, $price);
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }
}