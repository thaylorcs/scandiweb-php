<?php

namespace Scandiweb\Entities;

class Furniture extends Product
{
    private string $height;
    private string $width;
    private string $length;

    public function __construct(string $sku, string $name, float $price, string $height, string $width, string $length)
    {
        parent::__construct($sku, $name, $price);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }
}