<?php

namespace Scandiweb\Service;

use InvalidArgumentException;
use Scandiweb\Repository\ProductRepository;

require_once __DIR__ . "/../../config/config.php";


class ProductService
{
    public const GET_RESOURCE = ['list'];
    public const DELETE_RESOURCE = ['delete'];
    public const POST_RESOURCE = ['create'];
    public const TABLE = 'products';

    private array $data = [];
    private array $requestBodyData = [];

    public function __construct($data = [])
    {
        $this->data = $data;
        $this->productRepository = new ProductRepository();
    }

    public function validateGet()
    {
        $response = null;
        $resource = $this->data['resource'];
        if (in_array($resource, self::GET_RESOURCE)) {
            $response = $this->data['id'] > 0 ? $this->getOneById() : $this->$resource();
        } else {
            throw new InvalidArgumentException("Resource Not found" . $this->data['id']);
        }

        if ($response === null) {
            throw new InvalidArgumentException("Invalid resource");
        }

        return $response;
    }

    public function validateDelete()
    {
        $response = null;
        $resource = $this->data['resource'];

        if (in_array($resource, self::DELETE_RESOURCE)) {
            if ($this->data['id'] > 0) {
                $response = $this->$resource();
            } else {
                throw new InvalidArgumentException("Id required");
            }
        } else {
            throw new InvalidArgumentException("Resource not found");
        }

        if ($response === null) {
            throw new InvalidArgumentException("Unable to get resource");
        }

        return $response;
    }

    public function validatePost()
    {
        $response = null;
        $resource = $this->data['resource'];
        if (in_array($resource, self::POST_RESOURCE)) {
            $response = $this->$resource();
        } else {
            throw new InvalidArgumentException("Resource Not found");
        }

        if ($response = null) {
            throw new InvalidArgumentException("Error trying to get the resource");
        }

        return $response;
    }

    public function setRequestBody($requestData)
    {
        $this->requestBodyData = $requestData;
    }

    private function getOneById()
    {
        return $this->productRepository->getById($this->data['id']);
    }

    private function list()
    {
        return $this->productRepository->allProducts();
    }

    public function create()
    {
        [$sku, $product_title, $price, $product_type, $product_attributes] = [$this->requestBodyData['sku'], $this->requestBodyData['product_title'], $this->requestBodyData['price'], $this->requestBodyData['product_type'], $this->requestBodyData['product_attributes']];

        if ($sku && $product_title && $price && $product_type) {
            if ($this->productRepository->save($sku, $product_title, $price, $product_type, $product_attributes) > 0) {
                $insertedId = $this->productRepository->getDb()->lastInsertId();
                $this->productRepository->getDb()->commit();
                return ['inserted_id' => $insertedId];
            }
            $this->productRepository->getDb()->rollBack();
        }
    }

    public function delete()
    {
        return $this->productRepository->remove($this->data['id']);
    }

}