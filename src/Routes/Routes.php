<?php

namespace Scandiweb\Routes;

class Routes
{
    public static function getRoutes(): array
    {
        $url = self::getUrl();

        switch ($url) {
            case "":
                require __DIR__ . "/../../public/index.php";
                break;
            default:
                $request = [];
                $request['route'] = strtolower($url[0]);
                $request['resource'] = $url[1] ?? null;
                $request['id'] = $url[2] ?? null;
                $request['method'] = $_SERVER['REQUEST_METHOD'];
        }
        return $request;
    }

    public static function getUrl()
    {
        $uri = $_SERVER['REQUEST_URI'];
        return explode('/', trim($uri, '/'));
    }
}