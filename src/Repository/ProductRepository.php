<?php

namespace Scandiweb\Repository;

use InvalidArgumentException;
use PDO;
use PDOException;
use const Scandiweb\Config\DB_NAME;
use const Scandiweb\Config\DB_USER;
use const Scandiweb\Config\HOST;
use const Scandiweb\Config\PASSWORD;

require_once __DIR__ . "/../../config/config.php";

class ProductRepository
{
    private PDO $db;

    public function __construct()
    {
        $this->db = $this->setDB();
    }

    public function setDb()
    {
        try {
            return new PDO('mysql:host=' . HOST . '; dbname=' . DB_NAME . ';', DB_USER, PASSWORD);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
    }

    public function allProducts(): array
    {
        $result = $this->db->query("SELECT * FROM products ORDER BY sku");
        $products = $result->fetchall($this->db::FETCH_ASSOC);

        return $products;
    }

    public function getById(int $id)
    {
        if ($id) {
            $query = ("SELECT * FROM products WHERE product_id = ?");
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(1, $id);
            $stmt->execute();
            $productCount = $stmt->rowCount();

            if ($productCount === 1) {
                return $stmt->fetch($this->db::FETCH_ASSOC);
            }

            throw new InvalidArgumentException("Product not found");
        }
        throw new InvalidArgumentException("Invalid Id");
    }

    public function save(string $sku, string $product_title, float $price, int $type, string $attributes): int
    {
        $insertProduct = "INSERT INTO products (sku, product_title, price, product_type, product_attributes) VALUES (?, ?, ?, ?, ?);";
        $this->getDb()->beginTransaction();
        $stmt = $this->getDb()->prepare($insertProduct);

        $stmt->bindValue(1, $sku);
        $stmt->bindValue(2, $product_title);
        $stmt->bindValue(3, $price);
        $stmt->bindValue(4, $type);
        $stmt->bindValue(5, $attributes);
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function remove($id)
    {
        $removeProduct = $this->db->prepare("DELETE FROM products WHERE product_id = ?");
        $removeProduct->bindParam(1, $id);
        $removeProduct->execute();
    }

    public function getDb()
    {
        return $this->db;
    }
}